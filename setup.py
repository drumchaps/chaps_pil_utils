from distutils.core import setup

setup(
    name="chaps_pil_utils",
    version="0.0.1",
    author="Chaps",
    author_email="drumchaps@gmail.com",
    maintainer="Chaps",
    maintainer_email="drumchaps@gmail.com",
    url="https://bitbucket.org/drumchaps/chaps_pil_utils",
    packages  = [
        "chaps_pil_utils",
    ],
    package_dir={'chaps_pil_utils': 'src/chaps_pil_utils'},
    #install_requires = ["requests",]
)


