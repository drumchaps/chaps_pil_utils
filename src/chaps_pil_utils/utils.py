# -*- coding: utf-8 -*-

import math, operator

from PIL import Image, ImageChops


def crop_and_save( source_path, dest_path, x1, y1, x2, y2 ):
    """ Crops a source image and saves the cropped image
    """
    tmpscreen = Image.open( source_path )
    tmpcrop = tmpscreen.crop( (x1, y1, x2, y2) )
    tmpcrop.save( dest_path )
    pass


def equalimages( path1, path2  ):
    """ Checks 2 paths and tests if they are identical.
    """
    im1 = Image.open( path1 )
    im2 = Image.open( path2 )
    return ImageChops.difference( im1, im2 ).getbbox() is None


def rmsdiff( path1, path2 ):
    im1 = Image.open( path1 )
    im2 = Image.open( path2 )
    h = ImageChops.difference( im1, im2 ).histogram()

    return math.sqrt(
        reduce(
	    operator.add,
            map(
	        lambda h, i: h*(i**2), h, range(256)
	    )
	) / ( float(im1.size[0]) * im1.size[1] ) 
    )
    pass
